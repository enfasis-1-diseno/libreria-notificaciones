package aplicacion;

import librerias.DecoradorFace;
import librerias.DecoradorSMS;
import librerias.DecoradorTeams;
import librerias.Notificador;

public class Cliente {
	
	private static boolean face = true;
	private static boolean sms = false;
	private static boolean teams = true;
	private static Notificador pila;

	public static void main(String[] args) {
		// TODO Auto-generated method stub	
		pila = new Notificador();
		if(face) {
			pila = new DecoradorFace(pila);
		}
		if(sms) {
			pila = new DecoradorSMS(pila);
		}
		if(teams) {
			pila = new DecoradorTeams(pila);
		}
		pila.enviar("Alerta de prueba del patr�n decorator");
	}	
}
