package librerias;

import patrones.NotificadorDecorador;

public class DecoradorFace extends NotificadorDecorador{

	public DecoradorFace(Notificador w) {
		super(w);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void enviar(String mensaje) {
		enviarPatron(mensaje);
		System.out.println("La notificación '" + mensaje + "' se envió por facebook");
	}
}
