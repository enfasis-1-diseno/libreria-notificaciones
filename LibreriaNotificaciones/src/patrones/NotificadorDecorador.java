package patrones;

import librerias.Notificador;

public abstract class NotificadorDecorador extends Notificador {
	
	private Notificador wrappee;
	
	public NotificadorDecorador(Notificador w) {
		this.wrappee = w;
	}
	
	public void enviarPatron(String mensaje) {
		wrappee.enviar(mensaje);
	}
	
}
