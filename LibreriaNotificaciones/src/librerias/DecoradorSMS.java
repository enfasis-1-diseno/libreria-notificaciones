package librerias;

import patrones.NotificadorDecorador;

public class DecoradorSMS extends NotificadorDecorador{

	public DecoradorSMS(Notificador w) {
		super(w);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void enviar(String mensaje) {
		enviarPatron(mensaje);
		System.out.println("La notificación '" + mensaje + "' se envió por mensaje de texto");
	}
	
}
